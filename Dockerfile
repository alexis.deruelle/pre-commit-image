ARG PYTHON_VERSION=3.12-alpine

FROM registry.hub.docker.com/library/python:${PYTHON_VERSION} as python

RUN mkdir /build

RUN apk add --no-cache git=2.43.0-r0 && \
    rm -rf /var/cache/apk/*

WORKDIR /build

COPY ./requirements.txt .

RUN pip install --no-cache-dir -r ./requirements.txt && \
    rm ./requirements.txt

COPY ./hook-install-config.yaml ./.pre-commit-config.yaml

RUN git init && \
    pre-commit install-hooks && \
    rm ./.pre-commit-config.yaml && \
    rm -rf .git

CMD ["--help"]

ENTRYPOINT ["pre-commit"]
