# pre-commit Docker image

This project builds a Docker image with pre-commit and some pre-commit hooks installed

It's aimed solely at being used in the pre-commit TBC template.
