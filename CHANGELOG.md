## [1.0.1](https://gitlab.com/alexis.deruelle/pre-commit-image/compare/1.0.0...1.0.1) (2024-04-06)


### Bug Fixes

* activate semantic-release-info job ([3a64484](https://gitlab.com/alexis.deruelle/pre-commit-image/commit/3a64484181af98d459d53cc1b925daae986b40ec))
* bumpversion.sh script ([b5ceb47](https://gitlab.com/alexis.deruelle/pre-commit-image/commit/b5ceb4727fc33b6429ca9c90da7b873051ba309e))

# 1.0.0 (2024-04-06)


### Bug Fixes

* add --no-cache to apk add ([9941a16](https://gitlab.com/alexis.deruelle/pre-commit-image/commit/9941a16185883c3ffb30444f8a117cf450df2a18))
* hadolint warnings ([9d7bbd1](https://gitlab.com/alexis.deruelle/pre-commit-image/commit/9d7bbd1fe9ad88ca1fb2a4827e3b84c7331441cb))
* reset CHANGELOG.md ([abc41f5](https://gitlab.com/alexis.deruelle/pre-commit-image/commit/abc41f5e116395977c5e5ee2ef4e701501c03a6a))


### Features

* add README.md and empty CHANGELOG.md ([1608d26](https://gitlab.com/alexis.deruelle/pre-commit-image/commit/1608d26280aa71a05df6d53ebe03a80e3f672dc2))
* create pre-commit image ([e620e49](https://gitlab.com/alexis.deruelle/pre-commit-image/commit/e620e49da50c8632842ab57c0ce7979321257579))
* fix release branch name ([41b580d](https://gitlab.com/alexis.deruelle/pre-commit-image/commit/41b580d90d770da1ae792e21ba17b90ffae09cde))
* realease configuration ([7e0b464](https://gitlab.com/alexis.deruelle/pre-commit-image/commit/7e0b464c1f777a680a84db9eaf74bcf82d30d59c))
